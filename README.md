# Ignition Error Pages

This integrates the [spatie/ignition package](https://github.com/spatie/ignition)
error handler with Drupal 10 and higher.

It is meant for development purposes - after enabling, you can get the
new error/exception page UI and add solutions to your custom exceptions
as shown in the screenshot. Do not enable this module on a production website,
as the exception/error output may show sensitive information.

See [this blog post](https://www.velir.com/ideas/2022/02/01/make-user-friendly-error-pages-in-drupal-10-with-ignition)
for some more information on why you may want to use this module.

## Table of contents

- Installation
- Configuration
- Maintainers


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).
Recommended way of installing module is via composer.


## Configuration

The Ignition error handling only works if
- The _Enable_ option is enabled through the settings form at
  `/admin/config/development/ignition`
- The Drupal log level is set to _All messages, with backtrace information_.
- The current user has the `view ignition error page` permission

As a general rule, you should always set the Drupal log level to _None_ on 
live/production environments. You can add a snippet like this to your
settings.php file to achieve this.

```php
if (SITE_IS_PROD) {
  $config['system.logging']['error_level'] = 'hide';
}
```

For Acquia environments, you can use following snippet.
```php
if (isset($_ENV['AH_SITE_ENVIRONMENT']) && $_ENV['AH_SITE_ENVIRONMENT'] === 'prod') {
  $config['system.logging']['error_level'] = 'hide';
}
```

The color theme and the editor used for the code preview can be configured by
clicking the cog icon in the top right corner of the error page. The settings
are stored in the user or session storage, so they will be remembered across
multiple requests.

The _Store settings in ~/.ignition.json_ option in the settings form at
`/admin/config/development/ignition` can be used to store the Ignition settings 
in a file in the user's home directory. This is useful if you want to share the
same settings across multiple projects. This is only recommended for local,
non-shared development environments.

## Maintainers

- Dieter Holvoet - [DieterHolvoet](https://www.drupal.org/u/dieterholvoet)
- Kevin Quillen - [kevinquillen](https://www.drupal.org/u/kevinquillen)
- Gaurav Kapoor - [gaurav.kapoor](https://www.drupal.org/u/gauravkapoor)

**Supporting organization:**
- [Velir](https://www.drupal.org/velir)
