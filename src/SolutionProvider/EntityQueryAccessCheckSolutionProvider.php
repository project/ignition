<?php

namespace Drupal\ignition\SolutionProvider;

use Drupal\Core\Entity\Query\QueryException;
use Spatie\Ignition\Contracts\BaseSolution;
use Spatie\Ignition\Contracts\HasSolutionsForThrowable;

/**
 * Provides a solution for missing accessCheck calls on entity queries.
 */
class EntityQueryAccessCheckSolutionProvider implements HasSolutionsForThrowable {

  /**
   * {@inheritdoc}
   */
  public function canSolve(\Throwable $throwable): bool {
    if (!$throwable instanceof QueryException) {
      return FALSE;
    }

    if (!str_contains($throwable->getMessage(), 'Entity queries must explicitly set whether the query should be access checked or not')) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSolutions(\Throwable $throwable): array {
    return [
      BaseSolution::create('Add ->accessCheck() to the entity query.')
        ->setSolutionDescription("The \Drupal\Core\Entity\Query\QueryInterface::accessCheck() allows developers to specify whether only content entities that the current user has view access for should be returned when the query is executed. Since Drupal 10, all entity queries on content entities should always include an explicit call to ::accessCheck() prior to the query being executed.")
        ->setDocumentationLinks([
          'Drupal.org change record' => 'https://www.drupal.org/node/3201242',
        ]),
    ];
  }

}
