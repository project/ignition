<?php

namespace Drupal\ignition\SolutionProvider;

use Spatie\Ignition\Contracts\BaseSolution;
use Spatie\Ignition\Contracts\HasSolutionsForThrowable;

/**
 * Provides a solution for non-existing permissions being referenced in roles.
 */
class PermissionsMustExistSolutionProvider implements HasSolutionsForThrowable {

  /**
   * {@inheritdoc}
   */
  public function canSolve(\Throwable $throwable): bool {
    if (!$throwable instanceof \RuntimeException) {
      return FALSE;
    }

    if (!str_contains($throwable->getMessage(), 'Adding non-existent permissions to a role is not allowed')) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSolutions(\Throwable $throwable): array {
    return [
      BaseSolution::create('Remove the non-existent permissions from the role.')
        ->setSolutionDescription("Starting with Drupal 9.3.0, all permissions in a user role must be defined in a module.permissions.yml file or a permissions callback. Other permissions are now considered invalid. This includes permissions from uninstalled modules, permissions that depend on configuration that has been removed (such as a content type), and permissions of obscure origin.")
        ->setDocumentationLinks([
          'Drupal.org change record' => 'https://www.drupal.org/node/3193348',
        ]),
    ];
  }

}
