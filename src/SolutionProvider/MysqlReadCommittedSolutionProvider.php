<?php

namespace Drupal\ignition\SolutionProvider;

use Drupal\Core\Database\DatabaseExceptionWrapper;
use Spatie\Ignition\Contracts\BaseSolution;
use Spatie\Ignition\Contracts\HasSolutionsForThrowable;

/**
 * Provides a solution for common database deadlock errors.
 */
class MysqlReadCommittedSolutionProvider implements HasSolutionsForThrowable {

  /**
   * {@inheritdoc}
   */
  public function canSolve(\Throwable $throwable): bool {
    if (!$throwable instanceof DatabaseExceptionWrapper) {
      return FALSE;
    }

    if (!str_contains($throwable->getMessage(), 'Serialization failure: 1213 Deadlock found when trying to get lock')) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSolutions(\Throwable $throwable): array {
    return [
      BaseSolution::create('Use READ COMMITTED for MySQL transactions')
        ->setSolutionDescription("For high traffic sites the isolation level REPEATABLE READ will result in lots of deadlock errors. Since Drupal 9.4.0 and for MySQL, MariaDB or equivalent databases, the database connection array in the file settings.php has the isolation_level option. Set this to READ COMMITTED to fix this issue.")
        ->setDocumentationLinks([
          'Drupal.org change record' => 'https://www.drupal.org/node/3264101',
          'MySQL documentation' => 'https://dev.mysql.com/doc/refman/5.7/en/innodb-transaction-isolation-levels.html',
          'MariaDB documentation' => 'https://mariadb.com/kb/en/set-transaction/',
        ]),
    ];
  }

}
