<?php

namespace Drupal\ignition\SolutionProvider;

use Drupal\Core\Config\ConfigFactoryInterface;
use Psr\SimpleCache\CacheInterface;
use Spatie\Ignition\Solutions\OpenAi\OpenAiSolutionProvider as BaseSolutionProvider;

/**
 * Provides a service to get solutions from OpenAI.
 */
class OpenAISolutionProvider extends BaseSolutionProvider {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructs the configuration installer.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    CacheInterface $cache
  ) {
    $this->configFactory = $configFactory;
    $apiKey = $configFactory->get('ignition.settings')->get('open_ai_key') ?? '';

    parent::__construct($apiKey, $cache);
  }

  /**
   * {@inheritdoc}
   */
  public function canSolve(\Throwable $throwable): bool {
    $config = $this->configFactory->get('ignition.settings');
    return $config->get('open_ai') && $config->get('open_ai_key');
  }

}
