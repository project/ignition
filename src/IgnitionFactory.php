<?php

namespace Drupal\ignition;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserDataInterface;
use Spatie\Ignition\Config\FileConfigManager;
use Spatie\Ignition\Config\IgnitionConfig;
use Spatie\Ignition\Ignition;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * A factory for the Ignition service.
 */
class IgnitionFactory {

  /**
   * Constructs an IgnitionFactory object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\user\UserDataInterface $userData
   *   The user data service.
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The current session.
   * @param \Drupal\ignition\SolutionProviderCollector $solutionProviderCollector
   *   The solution provider collector.
   * @param \Spatie\Ignition\Config\FileConfigManager $fileConfigManager
   *   The file config manager.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected AccountProxyInterface $currentUser,
    protected UserDataInterface $userData,
    protected SessionInterface $session,
    protected SolutionProviderCollector $solutionProviderCollector,
    protected FileConfigManager $fileConfigManager,
  ) {
  }

  /**
   * Create a configured instance of the Ignition service.
   */
  public function make(): Ignition {
    $ignitionConfig = (new IgnitionConfig($this->getDefaultConfig()))
      ->merge($this->getUserConfig());
    $providers = $this->solutionProviderCollector->getSolutionProviders();

    return Ignition::make()
      ->applicationPath(DRUPAL_ROOT)
      ->setConfig($ignitionConfig)
      ->addSolutionProviders($providers);
  }

  /**
   * Get the sitewide, default config.
   */
  protected function getDefaultConfig(): array {
    $config = $this->configFactory->get('ignition.settings');

    return [
      'theme' => $config->get('dark_mode') ? 'dark' : 'auto',
    ];
  }

  /**
   * Get the per-user config.
   */
  protected function getUserConfig(): array {
    $config = $this->configFactory->get('ignition.settings');

    if ($config->get('store_settings_file')) {
      return $this->fileConfigManager->load();
    }

    if ($this->currentUser->isAuthenticated()) {
      return $this->userData->get(
          module: 'ignition',
          uid: $this->currentUser->id(),
          name: 'config',
      ) ?? [];
    }

    return $this->session->get('ignition_config', []);
  }

}
