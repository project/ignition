<?php

namespace Drupal\ignition;

use Spatie\Ignition\Contracts\HasSolutionsForThrowable;

/**
 * A service collector for Ignition solution providers.
 */
class SolutionProviderCollector {

  /**
   * The registered solution providers.
   *
   * @var \Spatie\Ignition\Contracts\HasSolutionsForThrowable[]
   */
  protected array $providers = [];

  /**
   * Register a solution provider.
   */
  public function addSolutionProvider(HasSolutionsForThrowable $provider): void {
    $this->providers[] = $provider;
  }

  /**
   * Get all registered solution providers.
   *
   * @return \Spatie\Ignition\Contracts\HasSolutionsForThrowable[]
   *   The solution providers.
   */
  public function getSolutionProviders(): array {
    return $this->providers;
  }

}
