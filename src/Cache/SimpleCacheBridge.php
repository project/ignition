<?php

namespace Drupal\ignition\Cache;

use Drupal\Core\Cache\CacheBackendInterface;
use Psr\SimpleCache\CacheInterface;

class SimpleCacheBridge implements CacheInterface {

  /**
   * Constructs a new SimpleCacheBridge object.
   *
   * @param CacheBackendInterface $cache
   *   The Drupal cache backend.
   */
  public function __construct(
    protected CacheBackendInterface $cache,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function get(string $key, mixed $default = null): mixed {
    return $this->cache->get($key)?->data ?? $default;
  }

  /**
   * {@inheritdoc}
   */
  public function set(string $key, mixed $value, \DateInterval|int|null $ttl = null): bool {
    $this->cache->set($key, $value, $this->getExpiration($ttl));
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(string $key): bool {
    $this->cache->delete($key);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function clear(): bool {
    $this->cache->deleteAll();
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getMultiple(iterable $keys, mixed $default = null): iterable {
    $cids = [...$keys];
    return $this->cache->getMultiple($cids) ?: $default;
  }

  /**
   * {@inheritdoc}
   */
  public function setMultiple(iterable $values, \DateInterval|int|null $ttl = null): bool
  {
    $items = [];
    foreach ($values as $cid => $value) {
      $items[$cid][] = [
        'data' => $value,
        'expire' => $this->getExpiration($ttl),
      ];
    }

    $this->cache->setMultiple($items);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultiple(iterable $keys): bool
  {
    $cids = [...$keys];
    $this->cache->deleteMultiple($cids);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function has(string $key): bool
  {
    return $this->cache->get($key) !== FALSE;
  }

  /**
   * Converts a TTL to an expiration timestamp.
   *
   * @param \DateInterval|int|null $ttl
   *   The TTL.
   *
   * @return int|null
   *   The expiration timestamp, or NULL if no expiration.
   */
  protected function getExpiration(\DateInterval|int|null $ttl): ?int {
    $now = new \DateTime(timezone: new \DateTimeZone('UTC'));

    if (is_int($ttl)) {
      return $now->add(new \DateInterval("PT{$ttl}S"))->getTimestamp();
    }

    if ($ttl instanceof \DateInterval) {
      return $now->add($ttl)->getTimestamp();
    }

    return NULL;
  }
}
