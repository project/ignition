<?php

namespace Drupal\ignition\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Error;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Spatie\Ignition\Ignition;

/**
 * Listener for handling PHP errors through Ignition.
 */
class ErrorHandlerSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Ignition service.
   *
   * @var \Spatie\Ignition\Ignition
   */
  protected $ignition;

  /**
   * ErrorHandlerSubscriber constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory for the form.
   * @param \Spatie\Ignition\Ignition $ignition
   *   The Ignition service.
   */
  public function __construct(AccountProxyInterface $account, ConfigFactoryInterface $config_factory, Ignition $ignition) {
    $this->account = $account;
    $this->configFactory = $config_factory;
    $this->ignition = $ignition;
  }

  /**
   * Handle exceptions.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The event to process.
   */
  public function onKernelException(ExceptionEvent $event) {
    $exception = $event->getThrowable();

    if (!$this->shouldHandleException($exception)) {
      return;
    }

    // Get the output from Ignition.
    ob_start();
    $this->ignition->handleException($exception);
    $output = ob_get_clean();

    // Return Ignition's output as a Symfony response.
    $response = new Response($output);
    $event->setResponse($response);
  }

  /**
   * Determines whether the throwable should be handled by Ignition.
   *
   * @param \Throwable $throwable
   *   The throwable.
   *
   * @return bool
   *   TRUE if the throwable should be handled by Ignition, FALSE otherwise.
   */
  protected function shouldHandleException(\Throwable $throwable): bool {
    if (!$this->account->hasPermission('view ignition error page')) {
      return FALSE;
    }

    $ignition_enabled = $this->configFactory->get('ignition.settings')->get('enabled');
    if ($ignition_enabled != TRUE) {
      return FALSE;
    }

    $error_level = $this->configFactory->get('system.logging')->get('error_level');
    if ($error_level !== ERROR_REPORTING_DISPLAY_VERBOSE) {
      return FALSE;
    }

    $error = Error::decodeException($throwable);
    $error_displayable = error_displayable($error);

    if ($error_displayable === FALSE) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::EXCEPTION][] = ['onKernelException', -255];
    return $events;
  }

}
