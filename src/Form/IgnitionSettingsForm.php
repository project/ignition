<?php

namespace Drupal\ignition\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Configure Ignition settings for this site.
 */
class IgnitionSettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->moduleHandler = $container->get('module_handler');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ignition_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ignition.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory()->get('ignition.settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('enabled'),
      '#description' => $this->t('Warning: Only enable for development environments where you need debugging information.'),
    ];

    $form['dark_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Dark mode'),
      '#default_value' => $config->get('dark_mode'),
      '#description' => $this->t('Enable dark mode for the Ignition error page.'),
    ];

    $form['store_settings_file'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Store settings in ~/.ignition.json'),
      '#default_value' => $config->get('store_settings_file'),
      '#description' => $this->t("Store the Ignition settings file in the user's home directory. This will allow you to share your settings across projects."),
    ];

    $form['open_ai'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('OpenAI'),
      '#default_value' => $config->get('open_ai'),
      '#description' => $this->t('Enable OpenAI solution provider.'),
    ];

    $form['open_ai_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI API key'),
      '#default_value' => $config->get('open_ai_key'),
      '#description' => $this->t('Get an API key @link.', [
        '@link' => Link::fromTextAndUrl(
          $this->t('here'),
          Url::fromUri('https://platform.openai.com/account/api-keys', [
            'attributes' => ['target' => '_blank'],
          ])
        )->toString(),
      ]),
      '#states' => [
        'visible' => [
          ':input[name="open_ai"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="open_ai"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Communicate overridden options.
    foreach (Element::children($form) as $key) {
      if ($this->moduleHandler->moduleExists('coi')) {
        $form[$key]['#config'] = [
          'key' => 'ignition.settings:' . $key,
          'secret' => TRUE,
        ];
      }
      elseif ($config->hasOverrides($key)) {
        $form[$key]['#disabled'] = TRUE;
        $form[$key]['#description'] = $this->t('This option is overridden, likely in settings.php, and cannot be changed here.');
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->get('ignition.settings');

    // Don't save overridden options.
    foreach (array_keys($config->get()) as $key) {
      if ($config->hasOverrides($key)) {
        $form_state->unsetValue($key);
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('ignition.settings')
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('dark_mode', $form_state->getValue('dark_mode'))
      ->set('store_settings_file', $form_state->getValue('store_settings_file'))
      ->set('open_ai', $form_state->getValue('open_ai'))
      ->set('open_ai_key', $form_state->getValue('open_ai_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
