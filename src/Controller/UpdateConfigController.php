<?php

namespace Drupal\ignition\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserDataInterface;
use Spatie\Ignition\Config\FileConfigManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Stores the Ignition config in user or session data.
 */
class UpdateConfigController implements ContainerInjectionInterface {

  /**
   * UpdateConfigController constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\user\UserDataInterface $userData
   *   The user data service.
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The current session.
   * @param \Spatie\Ignition\Config\FileConfigManager $fileConfigManager
   *   The file config manager.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected AccountProxyInterface $currentUser,
    protected UserDataInterface $userData,
    protected SessionInterface $session,
    protected FileConfigManager $fileConfigManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('user.data'),
      $container->get('session'),
      $container->get('ignition.file_config_manager'),
    );
  }

  /**
   * Stores the Ignition config in user or session data.
   */
  public function __invoke(Request $request): JsonResponse {
    $config = $this->configFactory->get('ignition.settings');
    $data = json_decode($request->getContent(), TRUE);

    if ($config->get('store_settings_file')) {
      // Save to ~/.ignition.json.
      $this->fileConfigManager->save($data);
    }
    elseif ($this->currentUser->isAuthenticated()) {
      // Save to user data.
      $this->userData->set(
        module: 'ignition',
        uid: $this->currentUser->id(),
        name: 'config',
        value: $data,
      );
    }
    else {
      // Save to session.
      $this->session->set('ignition_config', $data);
    }

    return new JsonResponse(TRUE);
  }

}
